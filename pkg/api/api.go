package api

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"

	"gitlab.com/idoko/swapiex/pkg/service"
	"gitlab.com/idoko/swapiex/pkg/swapi"
)

type Handler struct {
	svc    service.Service
	swapi  swapi.Swapi
	logger zerolog.Logger
}

func NewHandler(s swapi.Swapi, svc service.Service, logger zerolog.Logger) Handler {
	return Handler{
		swapi:  s,
		svc:    svc,
		logger: logger,
	}
}

func (h Handler) Register(g *gin.RouterGroup) {
	g.GET("/healthz", h.HealthCheck)
	g.GET("/force-refresh", h.ForceRefresh)

	g.GET("/movies", h.Movies)

	g.GET("/movies/:id/characters", h.GetCharacters)
	g.GET("/movies/:id/comments", h.GetComments)
	g.POST("/movies/:id/comments", h.AddComment)
}

func (h Handler) GetCharacters(c *gin.Context) {
	movieId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		h.logger.Info().Msg(fmt.Sprintf("error parsing movie id: %w", err))
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "could not parse movie id",
		})
	}

	sortKey := c.DefaultQuery("sort_key", "name")
	filter := c.DefaultQuery("filter", "")

	ch, err := h.swapi.GetCharacters(movieId, sortKey, filter)
	if err != nil {
		msg := "failed to get characters for selected movie"
		h.logger.Err(err).Msg(msg)
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": msg,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"count":           len(ch),
		"total_height_cm": h.svc.CalcTotalHeight(ch, "cm"),
		"total_height_in": h.svc.CalcTotalHeight(ch, "in"),
		"data":            ch,
	})
	return
}

func (h Handler) Movies(c *gin.Context) {
	movies, err := h.swapi.GetMovies()
	if err != nil {
		h.logger.Err(err).Msg("failed to fetch movies")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "failed to fetch movies",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": movies,
	})
	return
}

func (h Handler) GetComments(c *gin.Context) {
	movieId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		h.logger.Info().Msg(fmt.Sprintf("error parsing movie id: %w", err))
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "could not parse movie id",
		})
	}

	comments, err := h.svc.GetComments(movieId)
	c.JSON(http.StatusOK, gin.H{
		"data": comments,
	})
}
func (h Handler) HealthCheck(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"alive": "true",
	})
}

func (h Handler) AddComment(c *gin.Context) {
	commentReq := struct {
		Body string `json:"body"`
	}{}

	movieId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		h.logger.Info().Msg(fmt.Sprintf("error converting movie ID: %w", err))
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "nvalid movie id",
		})
		return
	}

	if err = c.ShouldBindJSON(&commentReq); err != nil {
		h.logger.Info().Msg(fmt.Sprintf("bad request body: %w", err))
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "invalid request",
		})
		return
	}

	if comment, err := h.svc.CreateComment(movieId, commentReq.Body, c.ClientIP()); err != nil {
		h.logger.Err(err).Msg("failed to save comment")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "could not save comment",
		})
		return
	} else {
		c.JSON(http.StatusCreated, gin.H{
			"message": "comment saved",
			"data":    comment,
		})

		return
	}
}

func (h Handler) ForceRefresh(c *gin.Context) {
	if err := h.swapi.ForceRefresh(); err != nil {
		h.logger.Err(err).Msg("refresh failed")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "refresh ok",
	})
	return
}
