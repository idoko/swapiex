package service

import (
	"fmt"
	"strconv"

	"github.com/rs/zerolog"
	"gitlab.com/idoko/swapiex/db"
	"gitlab.com/idoko/swapiex/pkg/swapi"
)

type Service struct {
	PgDb   db.PgDb
	Logger zerolog.Logger
	Swapi  swapi.Swapi
}

func (s Service) CreateComment(movieId int, body, ip string) (db.Comment, error) {
	c := db.Comment{
		Body:    body,
		MovieID: movieId,
		IP:      ip,
	}
	if err := s.PgDb.SaveComment(&c); err != nil {
		return c, err
	}
	go s.broadcastNewComment(c)
	return c, nil
}

func (s Service) GetComments(movieId int) ([]db.Comment, error) {
	return s.PgDb.GetComments(movieId)
}

func (s Service) broadcastNewComment(c db.Comment) {
	commentCount, err := s.PgDb.CommentCountFor(c.MovieID)
	if err != nil {
		s.Logger.Err(err).Msg("failed to get updated comment count")
		return
	}
	err = s.Swapi.SetCommentCount(c.MovieID, commentCount)
	if err != nil {
		s.Logger.Err(err).Msg("failed to update comment count")
	}
	return
}

func (s Service) CalcTotalHeight(chs []db.Character, scale string) string {
	total := 0
	for _, ch := range chs {
		height, err := strconv.Atoi(ch.Height)
		if err != nil {
			s.Logger.Err(err).Msg("failed to parse height")
			continue
		}
		total += height
	}
	if scale == "cm" {
		return fmt.Sprintf("%d cm", total)
	} else {
		in := float32(total) / float32(2.54)
		ft := float32(in) / float32(12)
		inr := (in - (12 * ft))
		return fmt.Sprintf("%f ft, %f inches", ft, inr)
	}
}
