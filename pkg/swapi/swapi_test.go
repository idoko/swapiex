package swapi

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/rs/zerolog"
	"gitlab.com/idoko/swapiex/db"
)

var (
	res = `{
	"count": 2, "next": null, "previous": null, 
	"results": [
		{"title": "ANH", "episode_id": 3, "opening_crawl": "hello swapi", "release_date": "2020-05-17", "url": "http://test",
		"characters": ["https://swapi.dev/api/people/1", "https://swapi.dev/api/people/4", "https://swapi.dev/api/people/9"]
		},
		{"title": "TESB", "episode_id": 4, "opening_crawl": "hello swapi", "release_date": "2020-05-15", "url": "http://test2",
		"characters": ["https://swapi.dev/api/people/7", "https://swapi.dev/api/people/3", "https://swapi.dev/api/people/9"]
		}
	]}
`
)

func makeSwapi() Swapi {
	dummyClient := redis.NewClient(&redis.Options{
		Addr:     "localhost:56888",
		Password: "",
		DB:       0,
	})
	logger := zerolog.New(os.Stdout).With().Timestamp().Logger()
	//todo: use a fake cache to test
	cache := db.NewRedisCache(dummyClient, logger)
	return NewSwapi(cache, logger)
}

func TestSwapi(t *testing.T) {
	s := makeSwapi()

	t.Run("extract movie from API response", func(t *testing.T) {

		movies, err := s.fetchMovies(context.Background(), []byte(res))
		if err != nil {
			t.Errorf("failed to fetch movies: %w", err)
		}

		if len(movies) != 2 {
			t.Errorf("expected %d movies, got %d", 2, len(movies))
		}

		expTime, _ := time.Parse(timeLayout, "2020-05-15")
		if !movies[1].ReleaseDate.Equal(expTime) {
			t.Errorf("incorrect release date for %s: expected %s, got %s", movies[1].Title, expTime, movies[1].ReleaseDate)
		}

		chars := movies[1].CharacterURLs
		if len(chars) != 3 {
			t.Errorf("expected %d characters for movie: %s, got %d", 3, movies[1].Title, len(chars))
		}

		expChar := "https://swapi.dev/api/people/7"
		if chars[0] != expChar {
			t.Errorf("expected first character url to be %s, got %s", expChar, chars[0])
		}

		s.logger.Info().Msg(fmt.Sprintf("movies: %+v", movies))
	})
}

func TestExtractId(t *testing.T) {
	s := makeSwapi()
	t.Run("extract movie ID from url", func(t *testing.T) {
		url := "https://swapi.dev/api/films/1/"
		id := s.extractID(url)
		if id != 1 {
			t.Errorf("expected movie ID to be %d, got %d", 1, id)
		}
	})

}
