package swapi

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/rs/zerolog"

	"gitlab.com/idoko/swapiex/db"
)

const (
	timeLayout = "2006-01-02"
)

type Swapi struct {
	cache  db.MovieCache
	logger zerolog.Logger
}

func NewSwapi(c db.MovieCache, logger zerolog.Logger) Swapi {
	return Swapi{
		cache:  c,
		logger: logger,
	}
}

func Init(c db.MovieCache, logger zerolog.Logger) (Swapi, error) {
	s := Swapi{
		cache:  c,
		logger: logger,
	}

	t, err := s.cache.ShouldRefresh(context.TODO())
	if err != nil {
		s.logger.Err(err).Msg("failed to get cache status")
		t = true
	}

	if !t {
		return s, nil
	}

	if err := s.refreshMovies(context.TODO()); err != nil {
		return s, errors.Wrap(err, "movie refresh failed")
	}

	return s, nil
}

func (s Swapi) ForceRefresh() error {
	return s.refreshMovies(context.TODO())
}

func (s Swapi) GetMovies() ([]db.Movie, error) {
	movies, err := s.cache.GetMovies(context.TODO())
	if err != nil {
		return movies, err
	}
	sort.Sort(db.MovieReleaseSorter(movies))
	return movies, nil
}

func (s Swapi) GetCharacters(movieId int, sortKey, filter string) ([]db.Character, error) {
	var chars []db.Character
	movie, err := s.cache.GetMovie(context.TODO(), movieId)
	if err != nil {
		return chars, err
	}
	urls := movie.CharacterURLs
	if len(urls) < 1 {
		//consider refreshing cache? for now we pretend there's no character in the movie
		return chars, nil
	}
	for _, url := range urls {
		ch, err := s.cache.GetCharacter(context.TODO(), url)
		if err != nil {
			s.logger.Err(err).Msg("fetching character from cache")
		}
		if ch.Name == "" {
			ch, err = s.fetchCharacter(context.TODO(), url, []byte{})
			if r := s.saveCharacter(ch); r != nil {
				s.logger.Err(err).Msg("could not save character to cache")
			}
		}
		if err != nil {
			s.logger.Err(err).Msg(fmt.Sprintf("could not get data for '%s', bailing", url))
			continue
		}
		chars = append(chars, ch)
	}

	chars = filterCharacters(chars, filter)

	if sortKey == "gender" {
		sort.Sort(db.CharGenderSorter(chars))
	} else if sortKey == "height" {
		sort.Sort(db.CharHeightSorter(chars))
	} else {
		sort.Sort(db.CharNameSorter(chars))
	}
	return chars, nil
}

func filterCharacters(chars []db.Character, gender string) []db.Character {
	if gender == "" {
		return chars
	}
	match := []db.Character{}
	for _, ch := range chars {
		if ch.Gender == gender {
			match = append(match, ch)
		}
	}
	return match
}

func (s Swapi) refreshMovies(ctx context.Context) error {
	movies, err := s.fetchMovies(ctx, []byte{})
	if err != nil {
		return err
	}

	for _, m := range movies {
		if err := s.cache.AddMovie(context.TODO(), m); err != nil {
			s.logger.Err(err).Msg(fmt.Sprintf("failed to save movie %s", m.Title))
		} else {
			s.logger.Info().Msg(fmt.Sprintf("cached movie %s", m.Title))
		}
	}
	return nil
}

func (s Swapi) SetCommentCount(movieId, commentCount int) error {
	movie, err := s.cache.GetMovie(context.TODO(), movieId)
	if err != nil {
		return err
	}
	movie.CommentCount = commentCount
	return s.cache.UpdateMovie(context.TODO(), movie)
}

func (s Swapi) getRequest(ctx context.Context, url string) ([]byte, error) {
	var jsonBody []byte
	req, err := http.NewRequestWithContext(ctx, "GET", url, nil)
	if err != nil {
		return jsonBody, err
	}
	req.Header.Set("Content-Type", "application/json")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return jsonBody, fmt.Errorf("failed to make http request: %w", err)
	}

	defer func() {
		if err := res.Body.Close(); err != nil {
			s.logger.Err(err).Msg("closing http response")
		}
	}()

	if res.StatusCode/100 == 2 {
		body, err := io.ReadAll(res.Body)
		if err != nil {
			return jsonBody, fmt.Errorf("failed to read response body: %w", err)
		}
		return body, nil
	} else {
		msg := fmt.Sprintf("swapi api request failed with status: %d", res.StatusCode)
		s.logger.Warn().Msg(msg)
		return jsonBody, fmt.Errorf(msg)
	}
}

func (s Swapi) fetchCharacter(ctx context.Context, charUrl string, resBody []byte) (db.Character, error) {
	var ch db.Character
	var err error

	if len(resBody) < 1 {
		resBody, err = s.getRequest(ctx, charUrl)
		if err != nil {
			return ch, err
		}
	}
	if err := json.Unmarshal(resBody, &ch); err != nil {
		return ch, errors.Wrap(err, "parse character response")
	}
	return ch, nil
}

func (s Swapi) saveCharacter(ch db.Character) error {
	if err := s.cache.AddCharacter(context.TODO(), ch); err != nil {
		s.logger.Err(err).Msg(fmt.Sprintf("failed to save character %s", ch.Name))
		return err
	} else {
		s.logger.Info().Msg(fmt.Sprintf("cached character %s", ch.Name))
	}
	return nil
}

func (s Swapi) fetchMovies(ctx context.Context, resBody []byte) ([]db.Movie, error) {
	var movies []db.Movie
	var err error

	type movieResult struct {
		Title        string   `json:"title"`
		ReleaseDate  string   `json:"release_date"`
		Characters   []string `json:"characters"`
		OpeningCrawl string   `json:"opening_crawl"`
		URL          string   `json:"url"`
	}

	jsonResponse := struct {
		Count   int           `json:"count"`
		Results []movieResult `json:"results"`
	}{}

	if len(resBody) < 1 {
		swapiUrl := "https://swapi.dev/api/films"
		resBody, err = s.getRequest(ctx, swapiUrl)
		if err != nil {
			return movies, err
		}
	}
	if err := json.Unmarshal(resBody, &jsonResponse); err != nil {
		return movies, errors.Wrap(err, "parse response")
	}
	results := jsonResponse.Results

	if len(results) < 0 {
		return movies, fmt.Errorf("movie result set is empty")
	}

	for _, v := range results {
		var mv db.Movie
		mv.Title = v.Title
		mv.OpeningCrawl = v.OpeningCrawl
		mv.ReleaseDate, _ = time.Parse(timeLayout, v.ReleaseDate)
		mv.URL = v.URL
		mv.CharacterURLs = v.Characters
		mv.ID = s.extractID(mv.URL)
		movies = append(movies, mv)
	}

	return movies, nil
}

func (s Swapi) extractID(url string) int {
	pieces := strings.Split(url, "/")
	if len(pieces) < 5 {
		s.logger.Err(fmt.Errorf("not enough pieces in url: %v", pieces))
		return 0
	}
	id, err := strconv.Atoi(pieces[5])
	if err != nil {
		s.logger.Err(err).Msg("extracting id from url")
		return 0
	}
	return id
}
