# swapiex - SWAPI, but extended
An API providing data about Star Wars movies with support for comments and character filtering.

## Usage
### Requirements
To run the project, you will need:
- Docker and docker-compose installed locally.
- go-migrate (to help run the migrations). Alternatively, you can `source` the sql files in the "sql" folder to apply the migrations manually.
### Start the Services
Clone the repository and run the command below in the project directory.
```bash
$ docker-compose up --build
```
This will start the `redis`, `postgres` and the `api` services respectively.

### Database Setup/Migrations
With go-migrate installed and the docker-compose services running, run:
```bash
$ make migrate-up
```
The above will connect to the Postgres database and set up the `comments` table in the appropriate database schema.
### API Documentation
Below are the errors, endpoints, request signatures and sample responses for the swapiex endpoints.
## Errors
All errors are returned with the appropriate HTTP status codes. Typical errors are
- 400 Bad request
- 500 Internal server error.

In both of the above cases, the response body contains an "error" field that describes the reason for the error e.g
```json
{
    "error": "invalid request"
}
``` 
## `GET movies`
Retrieves a list of all movies, together with their opening crawls and comment
count.
None
### Query Params
None
### Response
```json
{
  "data": [
    {
      "id": 1,
      "title": "A New Hope",
      "opening_crawl": "It is a period of civil war.",
      "url": "https://swapi.dev/api/films/1/",
      "release_date": "1977-05-25T00:00:00Z",
      "comment_count": 3,
      "characters": [
        "https://swapi.dev/api/people/1/",
        "https://swapi.dev/api/people/2/",
        "https://swapi.dev/api/people/8/",
        "https://swapi.dev/api/people/9/"
      ]
    },
    {
      "id": 2,
      "title": "The Empire Strikes Back",
      "opening_crawl": "It is a period of civil war.",
      "url": "https://swapi.dev/api/films/2/",
      "release_date": "2000-05-25T00:00:00Z",
      "comment_count": 2,
      "characters": [
        "https://swapi.dev/api/people/1/",
        "https://swapi.dev/api/people/9/"
      ]
    }
  ]
}
```

## `POST movies/{id}/comments`
Adds a new comment for the movie with ID {id}
### Request Body
- `comment=[String]`: Required, the body of the comment. Character limit is 500
  characters.
### Query Params
None
### Response
```json
{
    "data": {
        "body": "how far, world",
        "created_at": "2021-01-01T00:00:00Z",
        "ip": "172.20.0.1",
        "movie_id": 1
    },
    "message": "comment saved"
}
```

## `GET movies/{id}/comments`
Retrieves all the comments for a movie
### Query Params
None
### Response
Array of comment objects with the following fields:
- `body=[String]`: Actual comment as posted.
- `created_at[String]`: Unix timestamp of the time the comment was posted.
- `id=[Number]`: Integer ID of the comment.
- `movie_id=[Number]`: The movie ID of the comment as seen on swapi.dev
- `ip=[String]`: IP address of the commenter.
```json

{
    "data": [
        {
            "body": "hello world, 3rd comment",
            "created_at": "2021-09-30T09:24:39.290803Z",
            "id": 3,
            "ip": "172.20.0.1",
            "movie_id": 1
        },
        {
            "body": "hello world, second comment",
            "created_at": "2021-09-30T09:02:16.10702Z",
            "id": 2,
            "ip": "172.20.0.1",
            "movie_id": 1
        }
    ]
}
```

## `GET movies/{id}/characters`
Retrieves all the characters in a movie
### Query Parameters
- `sort_key=[String]`: Field to sort characters by. Should be one of `name`, `gender`, or `height`. Defaults to `name`.
- `filter`=[String]: Only characters whose gender matches the value of filter
  will be shown. Should be one of `male`, `female`, or `n/a`. By default, no gender is
  filtered out.
### Response
The response contains:
- `count=[Number]`: Total number of characters in the result set
- `total_height_cm=[String]`: String representation of the total height of matched characters (in centimeters)
- `total_height_in=[String]`: String representation of the total height of matched characters (in feet and inches)
```json

{
  "count": 3,
  "total_height_cm": "3066 cm",
  "total_height_in": "100.590553 ft, 0.000000 inches",
    "data": [
        {
            "gender": "male",
            "height": "202",
            "name": "Darth Vader",
            "url": "https://swapi.dev/api/people/4/"
        },
        {
            "gender": "female",
            "height": "150",
            "name": "Leia Organa",
            "url": "https://swapi.dev/api/people/5/"
        },
        {
            "gender": "male",
            "height": "188",
            "name": "Raymus Antilles",
            "url": "https://swapi.dev/api/people/81/"
        }
    ]
}
```

## Milestones
Create a small set of rest API endpoints using Golang to do the following

- [x] List an array of movies containing the name, opening crawl and comment count
- [x] Add a new comment for a movie
- [x] List the comments for a movie
- [x] Get list of characters for a movie

### General Requirements
- [x]  The application should have basic documentation that lists available endpoints and methods along with their request and response signatures
- [x]  The exact api design in terms of total number of endpoints and http verbs is up to you
- [x]  Keep your application source code on a public Github repository. *Used GitLab instead*
- [ ]  Deploy the API endpoints and provide a live demo URL of the API documentation. Heroku is a good option. 
- [x]  Bonus, but not mandatory, if you can dockerize the development environment _maybe dockerize migrate command too_

### Data requirements

- [x] The movie data should be fetched online from `**[<https://swapi.dev>](<https://swapi.dev>)**`
- [x]  Movie names in the movie list endpoint should be sorted by release date from earliest to newest and each movie should be listed along with opening crawls and count of comments.
- [x]  Data fetched from `**[<https://swapi.dev>](<https://swapi.dev>)`** should be cached with Redis and then accessed from the cache for subsequent requests.
- [x]  Comments should be stored in a Postgres database.
- [x]  Error responses should be returned in case of errors.

### Character list requirements

- [x]  Endpoint should accept sort parameters to sort by one of name, gender or height in ascending or descending order.
- [x]  Endpoint should also accept a filter parameter to filter by gender
- [x]  The response should also return metadata that contains the total number of characters that match the criteria along with the total height of the characters that match the criteria
- [x]  The total height should be provided both in cm and in feet/inches. For instance, 170cm makes 5ft and 6.93 inches.

### Comment requirements

- [x]  Comment list should be retrieved in reverse chronological order
- [x]  Comments should be retrieved along with the public IP address of the commenter and UTC date&time they were stored
- [x]  Comment length should be limited to 500 characters
