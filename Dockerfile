FROM golang:alpine AS builder
MAINTAINER Michael Okoko <me@mchl.xyz>

COPY go.mod go.sum /go/src/gitlab.com/idoko/swapiex/
WORKDIR /go/src/gitlab.com/idoko/swapiex
RUN go mod download
COPY . /go/src/gitlab.com/idoko/swapiex/
RUN CGO_ENABLED=0 GOOS=linux go build -a -o build/swapiex ./cmd/swapiex

FROM alpine
RUN apk add --no-cache ca-certificates && update-ca-certificates
COPY --from=builder /go/src/gitlab.com/idoko/swapiex/build/swapiex /usr/bin/swapiex

EXPOSE 6060
ENTRYPOINT ["/usr/bin/swapiex"]

