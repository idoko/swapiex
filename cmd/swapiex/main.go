package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	"github.com/rs/zerolog"

	"gitlab.com/idoko/swapiex/db"
	"gitlab.com/idoko/swapiex/pkg/api"
	"gitlab.com/idoko/swapiex/pkg/service"
	"gitlab.com/idoko/swapiex/pkg/swapi"
)

func apiService() (service.Service, error) {
	var postgresPort int
	var err error
	var svc service.Service
	postgresPort, err = strconv.Atoi(os.Getenv("POSTGRES_DB_PORT"))
	if err != nil {
		return svc, err
	}

	pgLogger := createLogger("postgres")
	svcLogger := createLogger("api-service")
	pgConfig := db.PgConfig{
		Host:     os.Getenv("POSTGRES_DB_HOST"),
		Port:     postgresPort,
		Username: os.Getenv("POSTGRES_USER"),
		Password: os.Getenv("POSTGRES_PASSWORD"),
		DbName:   os.Getenv("POSTGRES_DB"),
		Logger:   pgLogger,
	}

	pg, err := db.NewPg(pgConfig)
	if err != nil {
		return svc, err
	}
	svc = service.Service{
		PgDb:   pg,
		Logger: svcLogger,
	}
	return svc, nil
}

func main() {
	logger := createLogger("main")
	swapiLog := createLogger("swapi")
	apiLog := createLogger("api")

	rc, err := redisCache()
	if err != nil {
		logger.Err(err).Msg("establishing redis connection")
		os.Exit(1)
	}

	s, err := swapi.Init(rc, swapiLog)
	if err != nil {
		logger.Err(err).Msg("failed to refresh movies db")
		os.Exit(1)
	}

	svc, err := apiService()
	if err != nil {
		logger.Err(err).Msg("failed to set up service")
	}

	svc.Swapi = s
	handler := api.NewHandler(s, svc, apiLog)
	router := gin.Default()
	rg := router.Group("/api")
	handler.Register(rg)

	addr := fmt.Sprintf(":%d", 6060)
	srv := &http.Server{
		Addr:    addr,
		Handler: router,
	}

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			logger.Err(err).Msg("failed to start server")
		}
	}()

	<-ctx.Done()
	logger.Info().Msg("shutting down gracefully, press Ctrl+C again to force")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		logger.Fatal().Msg(fmt.Sprintf("Server forced to shutdown: %s", err))
	}
	logger.Info().Msg("exiting server")

}

func redisCache() (db.RedisCache, error) {
	var rc db.RedisCache
	redisClient := redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "",
		DB:       0,
	})

	if err := redisClient.Ping(context.Background()).Err(); err != nil {
		return rc, err
	}
	logger := createLogger("redis-cache")
	rc = db.NewRedisCache(redisClient, logger)

	return rc, nil
}

func createLogger(component string) zerolog.Logger {
	return zerolog.New(os.Stdout).With().Str("component", component).Timestamp().Logger()
}
