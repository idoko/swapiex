package db

import (
	"strconv"
	"time"
)

var (
	releaseDateLayout = "2006-01-02T15:04:05Z"
)

type Movie struct {
	ID            int       `json:"id"`
	Title         string    `json:"title"`
	OpeningCrawl  string    `json:"opening_crawl"`
	URL           string    `json:"url"`
	ReleaseDate   time.Time `json:"release_date"`
	CommentCount  int       `json:"comment_count"`
	CharacterURLs []string  `json:"characters"`
}

type MovieReleaseSorter []Movie

func (mrs MovieReleaseSorter) Len() int {
	return len(mrs)
}

func (mrs MovieReleaseSorter) Swap(i, j int) {
	mrs[i], mrs[j] = mrs[j], mrs[i]
}

func (mrs MovieReleaseSorter) Less(i, j int) bool {
	return mrs[i].ReleaseDate.Before(mrs[j].ReleaseDate)
}

type Comment struct {
	ID        int       `json:"id,omitempty"`
	MovieID   int       `json:"movie_id"`
	Body      string    `json:"body"`
	IP        string    `json:"ip"`
	CreatedAt time.Time `json:"created_at,omitempty"`
}

type Character struct {
	ID     int    `json:"id,omitempty"`
	Name   string `json:"name"`
	Height string `json:"height"`
	Gender string `json:"gender"`
	URL    string `json:"url"`
}

type CharNameSorter []Character
type CharGenderSorter []Character
type CharHeightSorter []Character

func (ns CharNameSorter) Len() int           { return len(ns) }
func (ns CharNameSorter) Swap(i, j int)      { ns[i], ns[j] = ns[j], ns[i] }
func (ns CharNameSorter) Less(i, j int) bool { return ns[i].Name < ns[j].Name }

func (gs CharGenderSorter) Len() int           { return len(gs) }
func (gs CharGenderSorter) Swap(i, j int)      { gs[i], gs[j] = gs[j], gs[i] }
func (gs CharGenderSorter) Less(i, j int) bool { return gs[i].Name < gs[j].Gender }

func (hs CharHeightSorter) Len() int      { return len(hs) }
func (hs CharHeightSorter) Swap(i, j int) { hs[i], hs[j] = hs[j], hs[i] }
func (hs CharHeightSorter) Less(i, j int) bool {
	var h1 int
	var h2 int
	h1, _ = strconv.Atoi(hs[i].Height)
	h2, _ = strconv.Atoi(hs[j].Height)

	return h1 < h2
}
