package db

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
)

const (
	moviesKey = "movies"
)

var (
	ErrNoRow = fmt.Errorf("no matching row")
)

type MovieCache interface {
	ShouldRefresh(ctx context.Context) (bool, error)
	UpdateMovie(ctx context.Context, movie Movie) error
	AddMovie(ctx context.Context, movie Movie) error
	GetMovie(ctx context.Context, movieId int) (Movie, error)
	GetMovies(ctx context.Context) ([]Movie, error)

	// GetCharacter uses the full character url as key to retrieve the character
	GetCharacter(ctx context.Context, charUrl string) (Character, error)
	AddCharacter(ctx context.Context, ch Character) error
}

type RedisCache struct {
	Client *redis.Client
	logger zerolog.Logger
}

func NewRedisCache(client *redis.Client, logger zerolog.Logger) RedisCache {
	return RedisCache{
		Client: client,
		logger: logger,
	}
}

func (rc RedisCache) GetCharacter(c context.Context, charUrl string) (Character, error) {
	var ch Character
	chStr, err := rc.Client.Get(c, charUrl).Result()
	if err != nil {
		if err == redis.Nil {
			return ch, ErrNoRow
		}
		return ch, err
	}

	if err := json.Unmarshal([]byte(chStr), &ch); err != nil {
		return ch, errors.Wrap(err, "marshalling character")
	}
	return ch, nil
}

func (rc RedisCache) AddCharacter(c context.Context, ch Character) error {
	b, err := json.Marshal(ch)
	if err != nil {
		return err
	}
	st := rc.Client.Set(c, ch.URL, string(b), 0)
	return st.Err()
}

func (rc RedisCache) ShouldRefresh(c context.Context) (bool, error) {
	// we could return true for instance if the list is over x days
	// for now, we just check if the movie set is empty
	res := rc.Client.SCard(c, moviesKey)
	if res == nil || res.Val() == 0 {
		rc.logger.Info().Msg("empty movie set, asking for refresh")
		return true, nil
	}
	if res.Err() != nil {
		rc.logger.Err(res.Err()).Msg("error while fetch movies, asking for refresh")
		return true, res.Err()
	}
	rc.logger.Info().Msg(fmt.Sprintf("found %d movies in cache. Will not refresh", res.Val()))
	return false, nil
}

func (rc RedisCache) UpdateMovie(c context.Context, m Movie) error {
	b, err := json.Marshal(m)
	if err != nil {
		return err
	}
	rc.Client.Set(c, movieInfoKey(m.ID), string(b), 0)
	return nil
}

func (rc RedisCache) GetMovies(c context.Context) ([]Movie, error) {
	movies := []Movie{}
	movieIDs, err := rc.Client.SMembers(c, moviesKey).Result()
	if err != nil {
		rc.logger.Err(err).Msg("error fetching movies")
		return movies, err
	}
	if len(movieIDs) < 1 {
		// empty movie list, maybe we could refresh but for now nope
		return movies, nil
	}

	for _, id := range movieIDs {
		mk, err := strconv.Atoi(id)
		if err != nil {
			rc.logger.Err(err).Msg("failed to get movie key")
			continue
		}
		mv, err := rc.GetMovie(c, mk)
		if err != nil {
			rc.logger.Err(err).Msg(fmt.Sprintf("failed to get movie with id:%d", mk))
			continue
		}
		movies = append(movies, mv)
	}
	return movies, nil
}

func (rc RedisCache) AddMovie(c context.Context, m Movie) error {
	b, err := json.Marshal(m)
	if err != nil {
		return err
	}

	rc.Client.Set(c, movieInfoKey(m.ID), string(b), 0)
	rc.Client.SAdd(c, moviesKey, m.ID)
	return nil
}

func (rc RedisCache) GetMovie(c context.Context, movieId int) (Movie, error) {
	key := movieInfoKey(movieId)
	var movie Movie
	movieStr, err := rc.Client.Get(c, key).Result()
	if err != nil {
		if err == redis.Nil {
			return movie, ErrNoRow
		}
		return movie, err
	}

	if err := json.Unmarshal([]byte(movieStr), &movie); err != nil {
		return movie, errors.Wrap(err, "marshalling movie")
	}
	return movie, nil
}

func movieInfoKey(id int) string {
	return fmt.Sprintf("%s:%d", moviesKey, id)
}
