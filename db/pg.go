package db

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
)

type PgConfig struct {
	Host     string
	Port     int
	Username string
	Password string
	DbName   string
	Logger   zerolog.Logger
}

type PgDb struct {
	Logger zerolog.Logger
	Conn   *sql.DB
}

func NewPg(cfg PgConfig) (PgDb, error) {
	db := PgDb{}
	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.Host, cfg.Port, cfg.Username, cfg.Password, cfg.DbName)
	conn, err := sql.Open("postgres", dsn)
	if err != nil {
		return db, err
	}
	db.Conn = conn
	db.Logger = cfg.Logger
	err = db.Conn.Ping()
	if err != nil {
		return db, err
	}
	return db, nil
}

func (p PgDb) SaveComment(comment *Comment) error {
	var id int
	var createdAt time.Time
	q := `INSERT INTO comments(movie_id, body, ip) VALUES($1, $2, $3) RETURNING id, created_at`
	err := p.Conn.QueryRow(q, comment.MovieID, comment.Body, comment.IP).Scan(&id, &createdAt)
	if err != nil {
		return errors.Wrap(err, "save comment")
	}
	return nil
}

func (p PgDb) CommentCountFor(movieId int) (int, error) {
	var commentCount int
	q := `SELECT COUNT(*) FROM comments WHERE movie_id = $1`
	err := p.Conn.QueryRow(q, movieId).Scan(&commentCount)
	if err != nil {
		return 0, errors.Wrap(err, "comment count")
	}
	return commentCount, nil
}

func (p PgDb) GetComments(movieId int) ([]Comment, error) {
	var comments []Comment
	var err error
	q := `SELECT id, movie_id, body, ip, created_at FROM comments WHERE movie_id=$1 ORDER BY id DESC`
	rows, err := p.Conn.Query(q, movieId)
	if err != nil {
		if err == sql.ErrNoRows {
			return comments, nil
		}
		return comments, errors.Wrap(err, "fetch comments")
	}

	for rows.Next() {
		var c Comment
		err := rows.Scan(&c.ID, &c.MovieID, &c.Body, &c.IP, &c.CreatedAt)
		if err != nil {
			p.Logger.Err(err).Msg("could not parse comment")
		} else {
			comments = append(comments, c)
		}
	}
	return comments, nil
}
